#!/bin/bash

print_usage(){
        echo
        echo
        echo "Usage:"
        echo "  " `basename "$0"` " <PAT token> <group id>  [-a] [-sg] [-d <email domain>] [-url <base url for api call>]"
        echo
        echo "Description:"
        echo "  This adhoc script uses GitLab API to get direct members from a group."
        echo "  If -a is provided it will get all memters including inherited and invited members"
        echo "	If -d is provided it will get only users matching the email domain."
        echo "  Script requires an PAT token to work"
        echo
        echo "Options:"
        echo "  -h:     Show this help"
        echo "  -a:     Get all members including inherited and invited members"
        echo "  -d:     Get direct members only"         
        echo "  -sg:    Use this option to get members from sub-groups too"
        echo "  -d:     Provide email domain to match users"
        echo "  -url:   Base URL for API call. default is gitlab.com"
        exit 1
}

get_emails(){
        while read -r id
        do
                member=$(curl -s --header "PRIVATE-TOKEN: $3" "$4/api/v4/users/$id")
                member_email=$(jq  ' . | ."email" ' <<< "$member")
                if [ -n "$2" ]
                then
                        if [[ "$member_email" =~ $email_regex ]] 
                        then
                                echo 'valid mail'
                                members+=("$member")
                        fi
                else 
                        members+=("$member")
                fi
        done < <(echo "$1"  |  jq -r '.[] | ."id" ' )
}

get_subgroups_emails() {
        sub_group_page=1
        until [ "$subgroup_response" == '[]' ]
        do
                subgroup_response=$(curl --fail -s --header "PRIVATE-TOKEN: $pat_token" "$BASE_URL/api/v4/groups/$group_id/subgroups?per_page=100&page=$sub_group_page" )
                if [[ ! $subgroup_response ]]
                then
                        echo "Failed to curl gropus/subgroups api, please check url $BASE_URL/api/v4/groups/$group_id/members?per_page=100&page=$pasub_group_pagee is valid, and your PAT has the correct access."
                        exit 1
                fi
                while read -r id
                do
                        echo "Getting emails for group " $id
                        subgroup_member_response=$(curl --fail -s --header "PRIVATE-TOKEN: $pat_token" "$BASE_URL/api/v4/groups/$id/members?per_page=100&page=$sub_group_page" )
                        # call function above to get emails
                        get_emails "$subgroup_member_response" "$email_regex" "$pat_token" "$BASE_URL"
                
                done < <(echo "$subgroup_response"  |  jq -r '.[] | ."id" ' )
                ((sub_group_page++))
        done 
}

BASE_URL="https://gitlab.com"
all=''
TTY=`tty`

if [  $# -le 1 ]
then
        print_usage
else
        # Assign first three arguments to variables
        pat_token=$1
        shift
        group_id=$1
        shift
        # Assignn rest of arguments
        while [ $# -gt 0 ]
        do
                case $1 in
                -a) all='/all'
                shift
                ;;
                -d) domain=$2
                shift
                ;;
                -url) BASE_URL=$2
                shift
                ;;
                -sg) get_subgroups=true
                shift
                ;;
                -h) print_usage
                ;;
                # Unrecognized option
                -*)
                        echo "$0: $1: unrecognized option" <&2
                        exit 1
                ;;
                **) break
                ;;
                esac
                shift
        done

fi

[ -n "$gropu_id"  ] && print_usage

page=1
email_regex="$domain"
members=()
filename="members_for_group_${group_id}.json"
[ -n "$domain" ] && filename="members_for_group_${group_id}_for_email_${domain}.com.json"

until [ "$curl_response" == '[]' ]
do
	curl_response=$(curl --fail -s --header "PRIVATE-TOKEN: $pat_token" "$BASE_URL/api/v4/groups/$group_id/members$all?per_page=100&page=$page" )
	if [[ ! $curl_response ]]
	then
		echo "Failed to curl gropus api, please check url $BASE_URL/api/v4/groups/$group_id/members$all?per_page=100&page=$page is valid, and your PAT has the correct access."
		exit 1
	fi

        # Get email from subgroups
        if [ -n "$get_subgroups" ] && [ "$get_subgroups" = true ]
        then
                echo "Getting members email for subgroups"
                get_subgroups_emails
                # get_emails "$curl_response" "$email_regex" "$pat_token" "$BASE_URL"
                get_subgroups=false
                echo "Done getting members email for subgroups"
        fi
        # Call function above to get emails
        echo "Getting members email for parent groups $group_id api page $page"
        get_emails "$curl_response" "$email_regex" "$pat_token" "$BASE_URL"
        echo "Done getting members email for parent groups $group_id api page $page"
	((page++))
done 
echo -n "[" > "$filename"
printf  "%s\n" "${members[@]}"| sort -u | tr -d '\n' >> "$filename"
echo -n "]" >> "$filename"
if [[ $OSTYPE == 'darwin'* ]]
then    
        # sed -i '' -e 's/\x0//d' "$filename" 
	sed -i '' -e "s.}{.},{.g" "$filename"
else 
        # sed  -i '' -e  's/\x0//g' "$filename"
	sed -i "s.}{.},{.g" "$filename"
fi
exit 0

