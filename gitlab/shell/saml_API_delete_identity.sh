#!/bin/bash 

print_usage(){
	echo
	echo
	echo "Usage:"
	echo "	" `basename "$0"` " <csv file> <PAT token> <group id> [-p <SAML provider>]"
	echo 
	echo "Description:"
	echo "	This adhoc script uses GitLab API to add SAML ID for users provided via csv file."
	echo "  CSV file needs the format email,SAML ID"
	echo "	Script requires the csv file and an ADMINSTRATOR PAT token to work"
	echo 
	echo "Options:"
	echo "	-h: 	show this help"
	echo "	-p: 	specify SAML provider. default "group_saml""
	echo "	-url: 	base URL for API call. default gitlab.com"
	exit 1
}

BASE_URL="https://gitlab.com"
provider="group_saml)'"
TTY=`tty`

if [  $# -le 2 ]
then
	print_usage
else
	# Assign first three arguments to variables
	file=$1
	shift
	pat_token=$1
	shift
	group_id=$1
	shift
	# Assignn rest of arguments
	while [ $# -gt 0 ]
	do 
		case $1 in 
		-p) provider=$2
		shift
		;;
		-url) BASE_URL=$2
                shift
                ;;
		-h) print_usage
		;;
		# Unrecognized option
		-*) 
			echo "$0: $1: unrecognized option" <&2
			exit 1
		;;
		**) break
		;;
		esac
		shift
	done

fi

if [ ! -f $file ]
then
        echo "csv file could not be found"
        exit 1
fi

[ -z $gropu_id  ] && print_usage

while read l
do
	[[ $l = \#* ]] && continue # skip commented line
	
	#user_id=$(echo $l  | cut -d, -f1)
	email=$(echo $l | cut -d, -f1)
	new_id=$(echo $l | cut -d, -f2)
	
	curl_output=$(curl -s  --header "PRIVATE-TOKEN: $pat_token" "$BASE_URL/api/v4/users?search=$email")
	user_id=$(echo $curl_output | jq '.[] | ."id"')
	username=$(echo $curl_output | jq '.[] | ."username"')
	
	echo
	if [ ! -z "$user_id" ]
	then
			echo "Updating SAML ID for user: $username"
			curl_response=$(curl -s -o /dev/null -w "%{response_code}" --request DELETE  --header "PRIVATE-TOKEN: $pat_token" "$BASE_URL/api/v4/users/${user_id}/identities/$provider")
		if [ $curl_response -eq 204 ]
		then
			echo "Succesfully updated SAML to : $new_id for user id $user_id"
		else
			echo -n "ERROR SAML: Identity could not be updated for SAML identity for user: $user_id , email: $email. Response code was: $curl_response ."
			echo "Command used 'curl -s -o /dev/null -w "%{response_code}" --request PUT  --header "PRIVATE-TOKEN: <pat_token>" "$BASE_URL/api/v4/users/${user_id}?extern_uid=${new_id}&provider=${provider}&group_id_for_sam=${group_di}")'"
		fi

	else
		echo "ERROR No SAML identity found for: $email" 
	fi

#	read -p  "Continue? [Y/N]" reply <$TTY 
#	if [[  $reply =~ ^[Nn]$ ]]
#	then
#    		exit 1
#	fi

done <  $file
