#!/bin/bash 

print_usage(){
        echo
        echo
        echo "Usage:"
        echo "  " `basename "$0"` " <PAT token> <group id> [-d <email domain>]"
        echo
        echo "Description:"
        echo "  This adhoc script uses GitLab API to get all members from a group."
        echo "  If -d is provided it will get only users matching the email domain."
        echo "  Script requires an PAT token to work"
        echo
        echo "Options:"
        echo "  -h:     show this help"
        echo "  -d:     Provide email domain to match users"
        echo "  -url:   base URL for API call. default gitlab.com"
        exit 1
}

TTY=`tty`

if [  $# -le 1 ]
then
        print_usage
else
        # Assign first three arguments to variables
        pat_token=$1
        shift
        API_URL=$1
        shift
        # Assignn rest of arguments
        while [ $# -gt 0 ]
        do
                case $1 in
                -l) limit=$2
                shift
                ;;
                -h) print_usage
		;;
                # Unrecognized option
                -*)
                        echo "$0: $1: unrecognized option" <&2
                        exit 1
                ;;
                **) break
                ;;
                esac
                shift
        done

fi


page=1
cloned=0

until [ "$curl_response" == '[]' ]
do
	curl_response=$(curl -s  --header "PRIVATE-TOKEN: $pat_token" "$API_URL/api/v4/projects?per_page=100&pages=$page" )
	while read -r project
	do
		if [ -n "$limit" ] && [ $cloned -ge "$limit" ] 
		then 
			break 2
		else
			p=$(echo $project | sed 's/"//g') # strip unecesary " 	
			git clone -v $p
			((cloned++))
			./generate_large_files.sh  1 largeFile txt -c -p ./$(basename $p .git)	-s 100 
		fi
	done < <( echo "$curl_response"  | jq '.[] | ."ssh_url_to_repo"' )
	((page++))
done
wait # wait for script running in background
