#!/bin/bash 

### TODO
# 1 Fix options handle for more generic
# 2  validate -g option to commit files

print_usage(){
	echo
        echo
        echo "Usage:"
	echo " `basename "$0"`  <number of files> <filename base> <file extension> [-c] [-s size] [-p path]"
	echo "Description:"
	echo "  This adhoc script creates the number of files specified in <nunber of files>."
	echo "  The files are created starting with <filename base> and with <file extension>."
	echo "  No need to include the '.' on <file extension>"
	echo
	echo "Options"
	echo "  -c:	Commit and push files to git project. Only use when working inside a git project"
	echo "  -s:	Integer, multiples of 1Kb, to define the file size to create. If not files created are 1Kb"
	echo "  -p:	Path where to create files. If not specified use current directory."


	exit 1
}

if [  $# -le 2 ]
then
        print_usage
else
        # Assign first two arguments to variables
	count=$1
	shift
	filenamebase=$1
	shift
	filenameextension=$1
	shift
        # Assignn rest of arguments
	while [ $# -gt 0 ]
        do
                case $1 in
                -c) commit=1
                # shift # This shift does not need to exists since -c does not have a second arg
                ;;
		-s) size=$2
		shift
		;;
		-p) path=$2
		shift
		;;
                -h) print_usage
                ;;
                # Unrecognized option
                -*)
                        echo "$0: $1: unrecognized option" <&2
                        exit 1
                ;;
                **) break
                ;;
                esac
                shift
        	done
fi

[ -z $size ] && size=1
[ -z $path ] && path=.

file_size=$(($size*512/512))

for (( filenumber = 1; filenumber <= $count ; filenumber++ )); do
	 dd if=/dev/urandom of=$path/$filenamebase$filenumber.$filenameextension bs=1024 count=$file_size
    
	 # commit files if -c is provided
	if ! [ -z $commit ]
	then
	    	git -C $path add $filenamebase$filenumber.$filenameextension
	    	git -C $path  commit -m"A random change in  $filenamebase$filenumber.$filenameextension" 
    		git -C $path push
	fi
done

