#!/bin/bash 

print_usage(){
	echo
	echo
	echo "Usage:"
	echo "	" `basename "$0"` " <csv file> <PAT token> [-p <SAML provider>]  [-scim <scim_token>] [-g <SCIM group path>]"
	echo 
	echo "Description:"
	echo "	This adhoc script uses GitLab API to update SAML and SCIM ID for users provided to csv file."
	echo "	Script requires the csv file and an ADMINSTRATOR PAT token to work"
	echo 
	echo "Options:"
	echo "	-h: 	show this help"
	echo "	-p: 	specify SAML provider. default "group_saml""
	echo "	-scim: 	provide SCIM token. if not provided only updates SAML ID"
	echo "	-g: 	profide gropu path to update SCIM ID. necesary to update SCIM"
	echo "	-url: 	base URL for API call. default gitlab.com"
	exit 1
}

BASE_URL="https://gitlab.com"
provider="group_saml"

if [  $# -le 1 ]
then
	print_usage
else
	# Assign first two arguments to variables
	file=$1
	shift
	pat_token=$1
	shift
	# Assignn rest of arguments
	while [ $# -gt 0 ]
	do 
		case $1 in 
  		-scim) scim_token=$2
		shift
		;;
		-p) provider=$2
		shift
		;;
		-g) group_path=$2
		shift
		;;
		-url) BASE_URL=$2
                shift
                ;;
		-h) print_usage
		;;
		# Unrecognized option
		-*) 
			echo "$0: $1: unrecognized option" <&2
			exit 1
		;;
		**) break
		;;
		esac
		shift
	done

fi

if [ ! -f $file ]
then
        echo "csv file could not be found"
        exit 1
fi

# Need to provide group path when using scim
if  [ ! -z $scim_token ]  && [  -z $group_path ] 
	then 
		print_usage
fi

while read l
do
	[[ $l = \#* ]] && continue # skip commented line
	
	old_id=$(echo $l  | cut -d, -f1)
	new_id=$(echo $l  | cut -d, -f2)
	# [ ! -z $scim_token ] && scim_id=$old_id
	
	curl_output=$(curl -s  --header "PRIVATE-TOKEN: $pat_token" "$BASE_URL/api/v4/users?extern_uid=${old_id}&provider=${provider}")
	user_id=$(echo $curl_output | jq '.[] | ."id"')
	username=$(echo $curl_output | jq '.[] | ."username"')
	echo
	if [ ! -z "$user_id" ]
	then
			echo "Updating SAML ID for user: $username"
			curl_response=$(curl -s -o /dev/null -w "%{response_code}" --request PUT  --header "PRIVATE-TOKEN: $pat_token" "$BASE_URL/api/v4/users/${user_id}?extern_uid=${new_id}&provider=${provider}")
		if [ $curl_response -eq 200 ]
		then
			echo "Succesfully updated SAML to : $new_id for user id $user_id"
		else
			echo -n "ERROR SAML: Identity could not be updated for SAML identity: $old_id. Response code was: $curl_response ."
			echo "Command used 'curl -s -o /dev/null -w "%{response_code}" --request PUT  --header "PRIVATE-TOKEN: <token>" "$BASE_URL/api/v4/users/${user_id}?extern_uid=${new_id}&provider=${provider}")'"
		fi
		if [ ! -z $scim_token ]
		then
                        echo "Updating SCIM ID for user: $username"
			scim_curl_response=$(curl -s -o /dev/null -w "%{response_code}"  --request PATCH "$BASE_URL/api/scim/v2/groups/$group_path/Users/$old_id" --header "Authorization: Bearer <scim_token>" --header "Content-Type: application/scim+json" --data "{ \"Operations\": [{\"op\":\"Add\",\"path\":\"id\",\"value\":\"$new_id\"}] }")
		if [ $curl_response -eq 200 ]
		then
			echo "Succesfully pdated SCIM id to : $new_id for user id $user_id"
		else
			echo -n "ERROR SCIM: Identity could not be updated for SCIM identity: $old_id. Response code was: $curl_response ."
			echo "Command used 'curl -s -o /dev/null -w "%{response_code}" --request PUT  --header "PRIVATE-TOKEN: $pat_token" "$BASE_URL/api/v4/users/${user_id}?extern_uid=${new_id}&provider=${provider}")'"
		fi

		fi
	else
		echo "ERROR No SAML identity found for: $old_id" 
	fi


done <  $file
