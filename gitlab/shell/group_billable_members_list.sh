#!/bin/bash

print_usage(){
        echo
        echo
        echo "Usage:"
        echo "  " `basename "$0"` " <PAT token> <group id> [-sg] [-d <email domain>] [-url <base url for api call>]"
        echo
        echo "Description:"
        echo "  This adhoc script uses GitLab API to get all members from a group."
        echo "	If -d is provided it will get only users matching the email domain."
        echo "  Script requires an PAT token to work"
        echo
        echo "Options:"
        echo "  -h:     Show this help"
        echo "  -d:     Provide email domain to match and filter users"
        echo "  -url:   Base URL for API call. default is gitlab.com"
        exit 1
}

get_emails(){
        while read -r id
        do
                member=$(curl -s --header "PRIVATE-TOKEN: $3" "$4/api/v4/users/$id")
                member_email=$(jq  ' . | ."email" ' <<< "$member")
                if [ -n "$2" ]
                then
                        if [[ "$member_email" =~ $email_regex ]] 
                        then
                                echo 'valid mail'
                                members+=("$member")
                        fi
                else 
                        members+=("$member")
                fi
        done < <(echo "$1"  |  jq -r '.[] | ."id" ' )
}


BASE_URL="https://gitlab.com"
TTY=`tty`

if [  $# -le 1 ]
then
        print_usage
else
        # Assign first three arguments to variables
        pat_token=$1
        shift
        group_id=$1
        shift
        # Assignn rest of arguments
        while [ $# -gt 0 ]
        do
                case $1 in
                -d) domain=$2
                shift
                ;;
                -url) BASE_URL=$2
                shift
                ;;
                -h) print_usage
                ;;
                # Unrecognized option
                -*)
                        echo "$0: $1: unrecognized option" <&2
                        exit 1
                ;;
                **) break
                ;;
                esac
                shift
        done

fi

[ -n "$gropu_id"  ] && print_usage

page=1
email_regex="$domain"
members=()
filename="billable_members_for_group_${group_id}.json"
[ -n "$domain" ] && filename="members_for_group_${group_id}_for_email_${domain}.com.json"

until [ "$curl_response" == '[]' ]
do
	curl_response=$(curl --fail -s --header "PRIVATE-TOKEN: $pat_token" "$BASE_URL/api/v4/groups/$group_id/billable_members?per_page=100&page=$page" )
	if [[ ! $curl_response ]]
	then
                echo
		echo "Failed to curl billable members api, please check:"
                echo "  1. the url $BASE_URL/api/v4/groups/$group_id/billable_members?per_page=100&page=$page is valid."
                echo "  2. the $group_id is a parent gropu."
                echo "  3. the PAT has the correct access, needs api read at least."
		exit 1
	fi
        # Call function above to get emails
        echo "Getting members email for parent groups $group_id api page $page"
        get_emails "$curl_response" "$email_regex" "$pat_token" "$BASE_URL"
        echo "Done getting members email for parent groups $group_id api page $page"
	((page++))
done 
echo -n "[" > "$filename"
printf  "%s\n" "${members[@]}"| sort -u | tr -d '\n' >> "$filename"
echo -n "]" >> "$filename"
if [[ $OSTYPE == 'darwin'* ]]
then    
        # sed -i '' -e 's/\x0//d' "$filename" 
	sed -i '' -e "s.}{.},{.g" "$filename"
else 
        # sed  -i '' -e  's/\x0//g' "$filename"
	sed -i "s.}{.},{.g" "$filename"
fi
exit 0

