#!/bin/bash 

print_usage(){
        echo
        echo
        echo "Usage:"
        echo "  " `basename "$0"` " <PAT token> <group id> [-d <email domain>]"
        echo
        echo "Description:"
        echo "  This adhoc script uses GitLab API to get all members from a group."
        echo "	If -d is provided it will get only users matching the email domain."
        echo "  Script requires an PAT token to work"
        echo
        echo "Options:"
        echo "  -h:     show this help"
        echo "  -d:     Provide email domain to match users"
        echo "  -url:   base URL for API call. default gitlab.com"
        exit 1
}

BASE_URL="https://gitlab.com"
provider="group_saml"
TTY=`tty`

if [  $# -le 1 ]
then
        print_usage
else
        # Assign first three arguments to variables
        pat_token=$1
        shift
        group_id=$1
        shift
        # Assignn rest of arguments
        while [ $# -gt 0 ]
        do
                case $1 in
                -d) domain=$2
                shift
                ;;
                -url) BASE_URL=$2
                shift
                ;;
                -h) print_usage
                ;;
                # Unrecognized option
                -*)
                        echo "$0: $1: unrecognized option" <&2
                        exit 1
                ;;
                **) break
                ;;
                esac
                shift
        done

fi

[ -n "$gropu_id"  ] && print_usage

page=1
email_regex="$domain"
members=()
filename="members_for_group_${group_id}.json"
[ -n "$domain" ] && filename="members_for_group_${group_id}_for_email_${domain}.com.json"

until [ "$curl_response" == '[]' ]
do
	curl_response=$(curl --fail -s --header "PRIVATE-TOKEN: $pat_token" "$BASE_URL/api/v4/groups/$group_id/members?per_page=100&page=$page" )
	if [[ ! $curl_response ]]
	then
		echo "Failed to curl gropus api, please check url $BASE_URL/api/v4/groups/$group_id/members?per_page=100&page=$page is valid, and your PAT has the correct access."
		exit 1
	fi
	while read -r id
	do
		member=$(curl -s --header "PRIVATE-TOKEN: $pat_token" "$BASE_URL/api/v4/users/$id")
		member_email=$(jq  ' . | ."email" ' <<< "$member")
		if [ -n "$domain" ]
		then
			if [[ "$member_email" =~ $email_regex ]] 
			then
				echo 'valid mail'
				members+=("$member")
			fi
		else 
			members+=("$member")
		fi
	done < <(echo "$curl_response"  |  jq -r '.[] | ."id" ' )
	((page++))
done 
echo -n "[" > "$filename"
printf  "%s" "${members[@]}" >> "$filename"
echo -n "]" >> "$filename"
if [[ $OSTYPE == 'darwin'* ]]
then
	sed -i '' -e "s/}{/},{/g" "$filename"
else 
	sed -i "s/}{/},{/g" "$filename"
fi
exit 0

