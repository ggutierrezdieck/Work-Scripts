#!/usr/local/bin/python

import ftplib
import argparse
import configparser
import sys, os

# TODO: Get only new files/ not existing in pwd
# TODO: handle errors
#           - Bad user   ftplib.error_perm: 530 Permission denied.
#           - Bad Password ftplib.error_perm: 530 Login incorrect.
#           - Bad IP : TimeoutError: [Errno 60] Operation timed out
# TODO: Get files after certain date
# TODO: Push files

def get_args(argv=None):
        parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
        parser.add_argument(
            "-c",
            "--configuration",
            help="Provide config file with host ip, username and password.",
            action="store",
            dest="config",
            default=None,
            nargs=1
            )
        parser.add_argument(
            "-u",
            "--user",
            help="Username t ologin to the server.",
            dest='user',
            default="",
            nargs=1,
         )
        parser.add_argument(
            "-p",
            "--password",
            help="Password to login to the server.",
            dest='password',
            default="",
            nargs=1,
         )
        parser.add_argument(
            "-f",
            "--filename",
            help="Specify the file name with files to transfer. It can be used multiple times for multiple files.",
            default="",
            nargs="*"
        )
        parser.add_argument(
            "--host",
            help="IP of the host to connect to. Default '34.170.124.50'.",
            default=["34.170.124.50"],
            nargs=1,
        )
        parser.add_argument(
            "-l",
            "--ls", 
            help="List files in FTP server director. Default /files directory",
            action="store_true",
            dest="ls",
            default=False
        )
        parser.add_argument(
            "-d",
            "--directory",
            help="List files in FTP server director. Default /files directory",
            default=["files"],
            nargs=1
        )
        parser.add_argument(
            "-a",
            "--all",
            help="Get all files, will re-download existing files in local dir",
            action="store_true",
            dest='all',
            default=False,
        )
        parser.add_argument(
            "--createConfig",
            help="Create a config file to use as a template",
            action="store_true",
            dest='create_config',
            default=False,
        )
        
        if len(sys.argv) == 1:
            parser.print_help()
            sys.exit()
        return parser.parse_args()


def load_configuration(config_file):
    try:
        config = configparser.ConfigParser()
        config.read_file(open(config_file))
    except FileNotFoundError:
        print("Configuration file " + config_file + " could not be found")
        sys.exit(1)
    except configparser.MissingSectionHeaderError:
        print("Configuration file shoudl have valid INI format, it is missing a Header in the format [<Header>]")
        sys.exit(1)
    except configparser.ParsingError:
        print("There was an error parcing " + config_file)
        sys.exit(1)
    return dict(config.items(config.sections()[0]))

def create_template_configuration(config_file, ip='34.170.124.50', username='<Username>', password='<Password>'):
    try:
        with open(config_file, 'w') as f:
            f.write('[FTP Configuration]\n')
            f.write('Host: ' + ip + '\n')
            f.write('Username: ' + username +' \n')
            f.write('Password: ' + password + '\n')
            f.close
        print("Configuration file created")
    except FileNotFoundError:
        print("Config file path could not be located")
        sys.exit(1)


class FTP(object):
    def __init__ (self, user, password, host):
        self.host = host
        self.user = user
        self.password = password

    def connect(self):
        self.ftp = ftplib.FTP(self.host)
        self.ftp.login(self.user,self.password)
        return self.ftp

    def cd(self, dir):
        self.ftp.cwd(dir)

    def ls(self, print=True):
        if print:
            self.ftp.retrlines('LIST')
        else:
            return self.ftp.nlst()
    
    def get_all_files(self):
        remote_files = self.ls(print=False)
        for file in remote_files:
            filename = file.strip()
            print("Getting filename: " + filename),  # The comma to suppress the extra new line char
            self.ftp.retrbinary("RETR " + filename ,open(filename, 'wb').write)
    
    def get_new_files(self):
        local_files = os.listdir()
        remote_files = self.ls(print=False) 
        for file in remote_files:
            if file in local_files:
                print("Skipping " + file + ", already exists locally")
                continue
            else:
                filename = file.strip()
                print("Getting filename: " + filename),  # The comma to suppress the extra new line char
                self.ftp.retrbinary("RETR " + filename ,open(filename, 'wb').write)


    def close(self):
        self.ftp.quit()


if __name__ == "__main__":
    args=get_args()
    
    if args.create_config:
        create_template_configuration("ftp.cfg")
        sys.exit()
        
    if args.config == None:
        user = args.user[0]
        password = args.password[0]
        host = args.host[0]
    else:
        config_dict = load_configuration(args.config[0])
        user = config_dict['username']
        password = config_dict['password']
        host = config_dict['host']
    
    connection = FTP(user, password, host)
    connection.connect()
    connection.cd(args.directory[0])
    if args.all:
        connection.get_all_files()
    else:
        connection.get_new_files()
    exit()
    if args.ls:
        connection.ls()
    else:
         connection.get_files()
    connection.close()

