#!/usr/bin/env ruby

# This Ruby script reads
# /var/log/gitlab/nginx/gitlab_access.log  and 
# /var/log/gitlab/gitlab-shell/gitlab-shell.log logs, combines them, 
# and calculates RPS, useful to compare against reference architecture
#
# example:
# > ruby get-rps.rb /var/log/gitlab/nginx/gitlab_access.log /var/log/gitlab/gitlab-shell/gitlab-shell.log
#
# API: 3.0, WEB: 2.3, GIT (Pull): 2.0, Git (Push): 1.0

require 'json'


class Array
    def mode
        max = 0
        c = Hash.new 0
        each {|x| cc = c[x] += 1; max = cc if cc > max}
        c.select {|k,v| v == max}.map {|k,v| k}
      end
end

    
git_push_shell = []  
git_pull_shell = []  
git_push_nginx = []  
git_pull_nginx = []  
api_calls_nginx = []
web_calls_nginx = []

class JSONType
    def self.valid?(value)
      result = JSON.parse(value)
  
      result.is_a?(Hash) || result.is_a?(Array)
    rescue JSON::ParserError, TypeError
      false
    end
end

def get_time_from_nginx_log(lines)
    times = []
    lines.each do |line|
        begin
            times << line.split("[").last.split("]").first.split(" ").first.split(":")[1..-1].join(":")
        rescue NoMethodError
            
        end    
    end 
    times
end

def get_time_from_json_log(lines)
    times = []
    lines.each do |line|
        begin
            times << line["time"].split("Z").first.split("T")[1..-1].join(":")
        rescue NoMethodError => e
            puts e
        end    
    end 
    times

end

def calculate_rps(nginx_lines = [], shell_lines =[])
    times = []
    sum = 0.0
    shell_times = get_time_from_json_log(shell_lines) unless shell_lines.empty?
    nginx_times = get_time_from_nginx_log(nginx_lines) unless nginx_lines.empty? 

    
    (times << shell_times << nginx_times).flatten!
    time_hash = times.tally

     # Calculating statistics
    time_values = time_hash.values
    mean = time_values.sum(0.0) / time_values.size
    sum = time_values.sum(0.0) { |element| (element - mean) ** 2 }
    variance = sum / (time_values.size - 1)
    standard_deviation = Math.sqrt(variance)

    begin
        pm = "\u00B1"
        return mean.nan? ? 0 : "#{mean.round(1)}"
        # return mean.nan? ? 0 : "RPS: #{mean.round(1)} #{pm.encode('utf-8')} #{standard_deviation.round(1)}, mode: #{time_values.mode} "
    rescue ZeroDivisionError
        
    end
    

end

if ARGV.empty?
    puts "Usage:"
    puts "\t #{__FILE__} <log file>"
    puts "\t Scan nginx/gitlab-access.log and/or gitlab-shell/gitlab-shell.log "
    puts "Example:"
    puts "\t #{__FILE__}  /var/log/gitlab/nginx/gitlab_access.log /var/log/gitlab/gitlab-shell/gitlab-shell.log"
    puts "\tOr:"
    puts "\t find . -name gitlab_access.log -o -name  gitlab-shell.log | xargs ruby #{__FILE__}"
    puts 
    exit
end

ARGV.each do |file|
    line = File.readlines(file).each do |line|
        
        if JSONType.valid?(line)
            line = JSON.parse(line) 
            begin
                case 
                when line["env"]["OriginalCommand"].include?("git-upload-pack")
                    git_pull_shell << line 
                
                when line["env"]["OriginalCommand"].include?("git-receive-pack")
                    git_push_shell << line 
                end
            rescue
                next
            end
            next
        elsif line =~ /^[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*/
            case 
            when line.include?("git-upload-pack") && line.include?('" 200')
                git_pull_nginx << line 
            
            when line.include?("git-receive-pack") && line.include?('" 200')
                git_push_nginx  << line 

            when line.include?("api")
                api_calls_nginx  << line
            
            else 
                web_calls_nginx  << line
            end 
        else
            puts "#{file} is not in valid Nginx log or GitLab Shell format"
            exit
        end
    end
end

puts "API: #{calculate_rps(api_calls_nginx)}, WEB: #{calculate_rps(web_calls_nginx)}, GIT (Pull): #{calculate_rps(git_pull_nginx, git_pull_shell)}, Git (Push): #{calculate_rps(git_push_nginx, git_push_shell)}"
