require 'gitlab'
require 'faker'

# Add config file to avoid secret leakage.
require '../../../configs/seed_config'
# Config file is as below.
#
# module Config
#   CLIENT = {
#     endpoint: 'https:/<host_url/api/v4',
#     private_token: '<private token>
#   }
# end


client = Gitlab.client(
  endpoint: Config::CLIENT[:endpoint], #
  private_token: Config::CLIENT[:private_token]
)

max_branch_count_per_project = 500 
commit_count_per_project = 500
group_name = 'Monorepo Group'
project_name = 'Monorepo'
gen_description = Faker::Hacker.say_something_smart +  Faker::Company.bs

# Color coding
RED = 31
BLUE = 34
def colorize(text, color_code)
  "\e[#{color_code}m#{text}\e[0m"
end

path = group_name.downcase.gsub(/[^0-9A-Za-z]/, '')
puts "Group -- #{group_name}/#{path}"
begin
  client.create_group(group_name, path)[0]
rescue => e
 puts "#{colorize('Error occurred while creating group.', RED)}"
 puts "#{'ERROR: ' + colorize(e.message , RED)}"
 puts "Searching for existing group: " + group_name
 puts "Found group: " + colorize(group.id.to_s, BLUE)  + ": " + colorize(group.name, BLUE)
end

group = client.group_search(group_name)[0]

puts "Project: #{project_name}"
options = {
  description: gen_description,
  default_branch: 'master',
  issues_enabled: 1,
  wiki_enabled: 1,
  merge_requests_enabled: 1,
  namespace_id: group.id
}
begin
 client.create_project(project_name, options)[0]
rescue => e
  puts "#{colorize('Error occurred while creating project.', RED)}"
  puts "#{colorize('ERROR: ' + e.message, RED)}"
  puts "Searching for existing project: " + project_name
  puts "Found project: " + colorize(project.id.to_s, BLUE) + " " + colorize(project.name, BLUE)
end

project = client.project_search(project_name)[0]

max_branch_count_per_project.times do

    branch_name =  [
      Faker::App.name,
      Faker::Food.spice,
      Faker::TvShows::SiliconValley.app,
      Faker::University.name,
      Faker::Science.element
    ].sample.downcase.gsub(/[^0-9A-Za-z]/, '')

    begin
      branch = client.create_branch(project.id, branch_name, 'main').name
    rescue => e
      puts "#{colorize('Error occurred while creating branch ' + branch_name, RED)}"
      puts "#{colorize('ERROR: ' + e.message, RED)}"
      branch = client.create_branch(project.id, branch_name + "-" +rand(99999).to_s, 'main').name
    else
      puts 'BRANCH: ' + branch + ' on PROJECT: ' + project.path
     end
  end

begin
 project_branches = client.branches(project.id).auto_paginate
rescue => e
  puts e.message
end

project_branches.each do |branch|
 commit_count_per_project.times do
  file_path = Faker::File.dir(segment_count: rand(0..2))
  file_name = [
    Faker::File.file_name(dir:"", ext: "txt", directory_separator: ''),
    Faker::Internet.slug,
    Faker::Lorem.word,
    Faker::Science.element,
    Faker::Science.element_state,
    Faker::Color.color_name
    ].sample
  file = file_path + file_name
  content = Faker::Lorem.paragraphs(number: rand(1..100)).join("\n")
  commit_message = Faker::Quote.yoda
  options = {
    author_name: Faker::Name.name,
    author_email: Faker::Internet.email
  }

    begin
      client.create_file(project.id, file_path + file_name, branch.name, content, commit_message, options)
    rescue => e
      puts "#{colorize('Error occurred while creating commit on branch ' + branch.name, RED)}"
      puts "#{colorize('ERROR: ' + e.message, RED)}"
    else 
      puts 'COMMIT: file ' + file_path + file_name + ' BRANCH: ' + branch.name + ' PROJECT: ' + project.path
    end
  end
end
