require 'gitlab'
require 'faker'

# Add config file to avoid secret leakage. 
require '../../../configs/seed_config'
# Config file is as below.
#
# module Config
#   CLIENT = {
#     endpoint: 'https:/<host_url/api/v4',
#     private_token: '<private token>
#   }
# end


client = Gitlab.client(
  endpoint: Config::CLIENT[:endpoint], # 
  private_token: Config::CLIENT[:private_token]
)


max_branch_count_per_project = 5 

client.projects.auto_paginate.each do |project| 
  rand(1..max_branch_count_per_project).times do
    
    branch_name =  [
      Faker::App.name,
      Faker::Food.spice,
      Faker::TvShows::SiliconValley.app,
      Faker::University.name,
      Faker::Science.element
    ].sample.downcase.gsub(/[^0-9A-Za-z]/, '')
        
    begin 
      branch = client.create_branch(project.id, branch_name, 'main').name
    rescue => e
      puts e
      branch = client.create_branch(project.id, branch_name + "-" +rand(99999).to_s, 'main').name
    end
      puts 'BRANCH: ' + branch + ' on PROJECT: ' + project.path
  end

end

commit_count_per_project = 50

client.projects.auto_paginate.each do |project|
  project_branches = client.branches(project.id).auto_paginate
  number_of_branches = project_branches.count
  rand(1..commit_count_per_project).times do
    file_path = Faker::File.dir(segment_count: rand(0..2))
    file_name = [ 
      Faker::File.file_name(dir:"", ext: "txt", directory_separator: ''),
      Faker::Internet.slug,
      Faker::Lorem.word,
      Faker::Science.element,
      Faker::Science.element_state,
      Faker::Color.color_name
      ].sample
    file = file_path + file_name
    content = Faker::Lorem.paragraphs(number: rand(1..100)).join("\n")
    commit_message = Faker::Quote.yoda
    options = {
      author_name: Faker::Name.name,
      author_email: Faker::Internet.email
    }
    branch = project_branches.sample.name

    begin
      client.create_file(project.id, file_path + file_name, branch, content, commit_message, options)
      puts 'COMMIT: file ' + file_path + file_name + ' BRANCH: ' + branch + ' PROJECT: ' + project.path
    rescue => e
      puts e.message
    end
  end
end
