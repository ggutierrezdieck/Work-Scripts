from requests import Session
from datetime import datetime, timedelta
# Use config for token. Reference https://medium.com/black-tech-diva/hide-your-api-keys-7635e181a06c
import config

# TODO: loop throug groups

groups = config.groups
cutoff_days = config.cutoff_days

headers={"PRIVATE-TOKEN": config.token_secret }
cutoff_date = datetime.today() + timedelta(days = cutoff_days)



def delete_projects(group):
    print("Deleting projects with activity prior to " + str(cutoff_date))
    page = 1
    while True:
        url = 'https://gitlab.com/api/v4/groups/' + str(group) + '/projects?per_page=100&page=' + str(page)
        session = Session()
        response = session.get(url, headers=headers )
        projects = response.json()
        if response.status_code == 200 and response.json() != []:
            for project in projects:
                project_id = project['id']
                created_date_string = project['created_at'].replace('T', ' ').replace('Z', '')
                last_activity_string = project['last_activity_at'].replace('T', ' ').replace('Z', '')                
                last_activity = datetime.strptime(last_activity_string,'%Y-%m-%d %H:%M:%S.%f')
                # created_date =  datetime.strptime(created_date_string,'%Y-%m-%d %H:%M:%S.%f')
        
                #if cutoff_date > created_date:
                if cutoff_date > last_activity:
                    print('Deleting project "' + str(project['name']) + '" with id: ' + str(project['id']) + ' created on ' + str(project['created_at']) + ' with last activiy on: ' + str(project['last_activity_at']))
                    url = 'https://gitlab.com/api/v4/projects/' + str(project_id)
                    session = Session()
                    # response = session.get(url, headers=headers )
                    response = session.delete(url, headers=headers )
                    print(str(project['name'] + ' deleted. Status response ' + str(response.json()['message'])))
        elif response.json() == []:
            break           
        else:
            print('Error during API call: response code: ' + str(response.status_code) + ', message:' + response.text)
        page = page + 1

def delete_subgroups(group):
    cutoff_date = datetime.today() + timedelta(days = cutoff_days + 10) # Cutoff date for groups is 10 days extra
    print("Deleting subgroups created prior to " + str(cutoff_date))

    
    page = 1
    while True:
        url = 'https://gitlab.com/api/v4/groups/' + str(group) + '/subgroups?per_page=100&page=' + str(page)
        session = Session()
        response = session.get(url, headers=headers )
        subgroups = response.json()
        if response.status_code == 200 and response.json() != []:
            for subgroup in subgroups:
                subgroup_id = subgroup['id']
                created_date_string = subgroup['created_at'].replace('T', ' ').replace('Z', '')
                created_date =  datetime.strptime(created_date_string,'%Y-%m-%d %H:%M:%S.%f')

                if cutoff_date > created_date:
                    print('Deleting subgroup"' + str(subgroup['name']) + '" with id: ' + str(subgroup['id']) + ' created on ' + str(subgroup['created_at']))
                    url = 'https://gitlab.com/api/v4/groups/' + str(subgroup_id)
                    session = Session()
                    #response = session.get(url, headers=headers )
                    response = session.delete(url, headers=headers )
                    print(str(subgroup['name']) + ' deleted. Status response ' + str(response.json()['message']))
        elif response.json() == []:
            break
        else:
            print('Error during API call: response code: ' + str(response.status_code) + ', message:' + response.text)
        page = page + 1


for group in groups:
    delete_subgroups(group)
    delete_projects(group)
